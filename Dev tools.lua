--Dev tools by Dienofail - v0.0`

--Changelog:

--v0.01 release 
	
local starttime = nil
local time1 = 0
local time2 = 0
function OnLoad()
	PrintChat("Dev tools by Dienofail v0.01 loaded")
	Config = scriptConfig("Dev tools settings", "Dev tools")
	Config:addParam("movonoff", "movement recorder on or off", SCRIPT_PARAM_ONOFF, false)
	Config:addParam("buffonoff", "buff printer on or off", SCRIPT_PARAM_ONOFF, true)
	Config:addParam("packetonoff", "packet on or off", SCRIPT_PARAM_ONOFF, true)
	Config:addParam("spellonoff", "spell on or off", SCRIPT_PARAM_ONOFF, true)
	starttime = os.clock()
	file = io.open(SCRIPT_PATH .. "Movements.txt", "w")
end

function writeLine(file, line)
	file:write(line.."\n")

end


function OnTick()
	if Config.movonoff then
		local elapsed_time = os.clock() - starttime 
		file:write(tostring(elapsed_time) .. "\n")
		for I = 1, heroManager.iCount do
			local hero = heroManager:GetHero(I)
			if hero.visible then
				file:write(tostring(hero.networkID) .. "\t" .. tostring(hero.x) .. "\t" .. tostring(hero.z) .. "\t" .. tostring(hero.ms) .. "\t" .. tostring(hero.charName) .. "\t" .. tostring(hero.isMe) .. "\n")
			else
				file:write(tostring(hero.networkID) .. "\t" .. "nil" .. "\n")
			end
		end
		file:write("\n")
	end
end

function OnGainBuff(unit, buff)
    if unit == myHero and Config.buffonoff then
        PrintChat(tostring(buff.name) .. ' gained')
			time1 = os.clock()
    end
end

function OnLoseBuff(unit, buff)
	if unit == myHero and Config.buffonoff then
			PrintChat(tostring(buff.name) .. ' lost')
			time2 = os.clock() - time1
			--PrintChat('took ' .. tostring(time2))
	end
end

function OnProcessSpell(unit, spell)
	if unit.isMe and Config.spellonoff then
		PrintChat(tostring(spell.name))
	end
end

function OnSendPacket(p)
--print("called")
	-- New handler for SAC: Reborn
	--if p:get("spellId") == SkillE.spellKey and not (AutoCarry.MainMenu.AutoCarry or AutoCarry.MainMenu.LaneClear or AutoCarry.MainMenu.MixedMode or AutoCarry.PluginMenu.SlowE) then
		--p:block()
	--end
		-- if p.header == 0x99 then
		-- 	PrintChat("0x99 SENT")
		-- end
		--[[if p.header == 0x9A and Config.packetonoff then
			p.pos = 5
			PrintChat('P.pos 5 for 0x9A is ' .. tostring(p:Decode1()))
		end]]
    if p.header == 0x99 and Config.packetonoff then --and Cast then -- 2nd cast of channel spells packet2
				p.pos = 1
				PrintChat("0x99 SENT")
        result = {
            dwArg1 = p.dwArg1,
            dwArg2 = p.dwArg2,
            sourceNetworkId = p:DecodeF(),
            spellId = p:Decode1(),
            fromX = p:DecodeF(),
            fromY = p:DecodeF(),
            toX = p:DecodeF(),
            toY = p:DecodeF(),
            targetNetworkId = p:DecodeF()
        }
				--file2:write(result)
        PrintChat(tostring(result.dwArg1))
				PrintChat(tostring(result.dwArg2))
				PrintChat(tostring(result.sourceNetworkId))
				PrintChat(tostring(result.spellId))
				PrintChat(tostring(result.fromX))
				PrintChat(tostring(result.fromY))
				PrintChat(tostring(result.toX))
				PrintChat(tostring(result.toY))
				PrintChat(tostring(result.targetNetWorkId))
    end
		
	  if p.header == 0xE5 and Config.packetonoff then --and Cast then -- 2nd cast of channel spells packet2
				PrintChat("0xE5 SENT")
        result = {
            dwArg1 = p.dwArg1,
            dwArg2 = p.dwArg2,
            sourceNetworkId = p:DecodeF(),
            spellId = p:Decode1(),
            fromX = p:DecodeF(),
            fromY = p:DecodeF(),
            toX = p:DecodeF(),
            toY = p:DecodeF(),
            --targetNetworkId = p:DecodeF()
        }
        PrintChat(tostring(result.dwArg1))
				PrintChat(tostring(result.dwArg2))
				PrintChat(tostring(result.sourceNetworkId))
				PrintChat(tostring(result.spellId))
				PrintChat(tostring(result.fromX))
				PrintChat(tostring(result.fromY))
				PrintChat(tostring(result.toX))
				PrintChat(tostring(result.toY))
				--PrintChat(tostring(result.targetNetWorkId))
    end
end